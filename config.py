# Setting up configuration

import os

basedir = os.path.abspath(os.path.dirname(__file__))
SQLQLCHEMY_ECHO = False
SQLALCHEMY_TRACK_MODIFICATIONS = True
SQLALCHEMY_DATABASE_URI = 'sqlite:///'+os.path.join(basedir, 'data.sqlite')
