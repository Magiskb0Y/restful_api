from flask import request
from flask_restful import Resource
from Model import Category
from Model import CategorySchema
from Model import db

category_schema = CategorySchema(many=False)
categories_schema = CategorySchema(many=True)

class CategoryResource(Resource):
    def get(self):
        categories = Category.query.all()
        categories = categories_schema.dump(categories).data
        return {'status': 'success', 'data': categories}, 200

    def post(self):
        json_data = request.get_json()
        if not json_data:
            return {'message': 'No input data privided'}, 400
        data, errors = category_schema.load(json_data)
        if errors:
            return errors, 422
        category = Category.query.filter_by(name=data['name']).first()
        if category:
            return {'message': 'Category already existed'}, 400
        category = Category(
            name=json_data['name']
        )
        db.session.add(category)
        db.session.commit()
        result = category_schema.dump(category).data
        return {'status': 'succes', 'data': result}, 201

    def put(self):
        json_data = request.get_json()
        if not json_data:
            return {'message': 'No input provided'}, 400
        data, errors = category_schema.load(json_data)
        if errors:
            return errors, 422
        category = Category.query.filter_by(name=data['name']).first()
        if not category:
            return {'message': 'Category does not existed'}, 400
        category.name = json_data['name']
        db.session.commit()
        result = category_schema.dump(category)
        return {'status': 'success', 'data': result}, 204

    def delete(self):
        json_data = request.get_json()
        if not json_data:
            return {'message': 'No input provided'}, 400
        data, errors = category_schema.load(json_data)
        if errors:
            return errors, 422
        category = Category.query.filter_by(id=data['id']).delete()
        db.session.commit()
        result = category_schema.dump(category)
        return {'status': 'success', 'data': result}, 204
