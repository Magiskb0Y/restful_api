from flask import request
from flask_restful import Resource
from Model import Comment
from Model import Category
from Model import CommentSchema
from Model import db

comment_schema = CommentSchema(many=False)
comments_schema = CommentSchema(many=True)


class CommentResource(Resource):
    def get(self):
        comments = Comment.query.all()
        comments = comments_schema.dump(comments).data
        return {'status': 'success', 'data': comments}, 200

    def post(self):
        json_data = request.get_json(force=True)
        if not json_data:
            return {'message': 'No input provided'}, 400
        data, errors = comment_schema.load(json_data)
        if errors:
            return errors, 422
        category_id = Category.query.filter_by(id=data['category_id']).first()
        if not category_id:
            return {'message': 'Not found category'}, 404
        comment = Comment(
            category_id=data['category_id'],
            comment=data['comment']
        )
        db.session.add(comment)
        db.session.commit()

        result = comment_schema.dump(comment).data
        return {'status': 'succes', 'data': result}, 201
